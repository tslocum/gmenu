# gmenu
[![CI status](https://gitlab.com/tslocum/gmenu/badges/master/pipeline.svg)](https://gitlab.com/tslocum/gmenu/commits/master)
[![Donate](https://img.shields.io/liberapay/receives/rocketnine.space.svg?logo=liberapay)](https://liberapay.com/rocketnine.space)

Desktop application launcher

Note: Linux is currently the only supported OS.

## Screenshots

### Console

![](https://gmenu.rocketnine.space/static/gmenu_v022_1.png)

![](https://gmenu.rocketnine.space/static/gmenu_v022_2.png)

### GUI

![](https://gmenu.rocketnine.space/static/gtkmenu_v022_1.png)

![](https://gmenu.rocketnine.space/static/gtkmenu_v022_2.png)

## Download

[**Download gmenu and gtkmenu binaries**](https://gmenu.rocketnine.space/download/?sort=name&order=desc)

## Compile

The following commands download and build gmenu from source. The resulting
binary is located at `~/go/bin/`.

### Console

```
go get gitlab.com/tslocum/gmenu/cmd/gmenu
```

### GUI

```
go get gitlab.com/tslocum/gmenu/cmd/gtkmenu
```

## Usage

Start typing the name of an application or shortcut and, when necessary, use the arrow keys to select the desired entry.

Press Enter to run normally, or press ALT+Enter to run in a terminal.

The input buffer may be executed as a shell command by selecting the last entry in the list.

## Support

Please share issues and suggestions [here](https://gitlab.com/tslocum/gmenu/issues).

## Integration Example - [sway](https://swaywm.org)/[i3](https://i3wm.org) + [alacritty](https://github.com/jwilm/alacritty)

### Console

```
bindsym $mod+d exec --no-startup-id alacritty --class gmenu --title gmenu --working-directory ~ -e gmenu
for_window [app_id="gmenu"] floating enable; resize set 750 260
```

### GUI

```
bindsym $mod+d exec --no-startup-id gtkmenu --width 500 --height 260
```
