package gmenu

// Config stores configuration variables.
type Config struct {
	PrintVersion bool

	DataDirs string

	HideGenericNames bool
	HideAppDetails   bool

	terminalCommand string
	browserCommand  string
}

// TerminalCommand returns the command to execute to open a terminal.
func (c *Config) TerminalCommand() string {
	if c.terminalCommand == "" {
		c.terminalCommand = "i3-sensible-terminal"
	}

	return c.terminalCommand
}

// BrowserCommand returns the command to execute to open a browser.
func (c *Config) BrowserCommand() string {
	if c.browserCommand == "" {
		c.browserCommand = "xdg-open"
	}

	return c.browserCommand
}

// VersionInfo is the text printed when the --version flag is supplied.
const VersionInfo = `%s - Desktop application launcher - v%s
https://gitlab.com/tslocum/gmenu
The MIT License (MIT)
Copyright (c) 2019 Trevor Slocum <trevor@rocketnine.space>
`

// Version is the version of the application.
var Version = "0.0.0"
