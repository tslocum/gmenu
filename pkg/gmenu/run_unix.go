// +build aix darwin dragonfly freebsd linux netbsd openbsd solaris

package gmenu

import (
	"fmt"
	"os"
	"os/exec"
	"syscall"
)

func run(config *Config, runScript string, path string, waitUntilFinished, runInTerminal bool) error {
	var cmd *exec.Cmd
	if runInTerminal {
		cmd = exec.Command(config.TerminalCommand(), "-e", runScript)
	} else {
		cmd = exec.Command("/usr/bin/env", "bash", "-c", runScript)
	}

	cmd.SysProcAttr = &syscall.SysProcAttr{Setpgid: true, Pgid: 0}
	cmd.Dir = path

	cmd.Stdin = os.Stdin
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr

	err := cmd.Start()
	if err != nil {
		return fmt.Errorf("failed to start command: %s", err)
	}

	if !waitUntilFinished {
		return nil
	}

	err = cmd.Wait()
	_, isExitErr := err.(*exec.ExitError)
	if err != nil && !isExitErr {
		return fmt.Errorf("failed to execute command: %s", err)
	}

	return nil
}
