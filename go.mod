module gitlab.com/tslocum/gmenu

go 1.12

require (
	github.com/gdamore/tcell/v2 v2.1.1-0.20201225194624-29bb185874fd
	github.com/gotk3/gotk3 v0.5.3-0.20210203060005-2f8426f58c71
	github.com/kballard/go-shellquote v0.0.0-20180428030007-95032a82bc51
	github.com/lithammer/fuzzysearch v1.1.1
	github.com/lucasb-eyer/go-colorful v1.2.0 // indirect
	github.com/mattn/go-isatty v0.0.12
	gitlab.com/tslocum/cview v1.5.3
	gitlab.com/tslocum/desktop v0.1.4
	golang.org/x/sys v0.0.0-20210124154548-22da62e12c0c // indirect
)
