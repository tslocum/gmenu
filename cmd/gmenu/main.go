package main

import (
	"flag"
	"fmt"
	"log"
	"os"

	"github.com/mattn/go-isatty"
	"gitlab.com/tslocum/gmenu/pkg/gmenu"
)

// Config stores configuration variables.
type Config struct {
	gmenu.Config

	EnableMouse bool
}

var (
	config = &Config{}

	done = make(chan bool)
)

func init() {
	gmenu.SharedInit(&config.Config)

	flag.BoolVar(&config.EnableMouse, "mouse", false, "enable mouse support")
}

func main() {
	flag.Parse()

	if config.PrintVersion {
		fmt.Printf(gmenu.VersionInfo, "gmenu", gmenu.Version)
		return
	}

	tty := isatty.IsTerminal(os.Stdout.Fd()) || isatty.IsCygwinTerminal(os.Stdout.Fd())
	if !tty {
		log.Fatal("failed to start gmenu: non-interactive terminals are not supported")
	}

	gmenu.LoadEntries(&config.Config)

	app, err := initTUI()
	if err != nil {
		log.Fatalf("failed to initialize terminal user interface: %s", err)
	}

	go func() {
		if err := app.Run(); err != nil {
			panic(err)
		}

		done <- true
	}()

	<-done

	closeTUI()
}
