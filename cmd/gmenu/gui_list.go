package main

import (
	"flag"
	"fmt"
	"strings"

	"github.com/kballard/go-shellquote"
	"gitlab.com/tslocum/desktop"
	"gitlab.com/tslocum/gmenu/pkg/gmenu"
)

func updateEntries(input string) {
	gmenu.FilterEntries()

	entryList.selected = 0
	entryList.origin = 0
	defer app.QueueUpdateDraw(updateEntryInfo)

	entryList.options = nil
	for _, entry := range gmenu.FilteredEntries {
		entryList.options = append(entryList.options, entry.Label)
	}
	if input != "" {
		entryList.options = append(entryList.options, input)
	}
}

func selectedIndex() int {
	if entryList == nil {
		return -1
	}

	return entryList.selected
}

func selectedEntry() *desktop.Entry {
	i := selectedIndex()
	if len(gmenu.FilteredEntries) == 0 || i < 0 || i > len(gmenu.FilteredEntries)-1 {
		return nil
	}

	return gmenu.FilteredEntries[i].Entry
}

func updateEntryInfo() {
	if config.HideAppDetails {
		return
	}

	var exLine, comLine string
	entry := selectedEntry()
	if entry != nil {
		if entry.Type == desktop.Application {
			exLine = entry.Exec
		} else { // Type == desktop.Link
			exLine = config.BrowserCommand() + " " + entry.URL
		}

		comLine = entry.Comment
	} else {
		exLine = fmt.Sprintf("bash -c %s", strings.TrimSpace(inputView.GetText()))

		comLine = "Shell command"
	}

	appDetailsView.SetText(exLine + "\n\n" + comLine)
}

func listSelect(runInTerminal bool) error {
	defer closeTUI()

	var (
		execute           string
		waitUntilFinished bool
	)
	entry := selectedEntry()
	if entry == nil {
		waitUntilFinished = true
		execute = inputView.GetText()
	} else if entry.Type == desktop.Application {
		if entry.Terminal {
			runInTerminal = true
		}

		execute = entry.ExpandExec(shellquote.Join(flag.Args()...))
	} else { // Type == desktop.Link
		execute = shellquote.Join(config.BrowserCommand(), entry.URL)
	}

	closeTUI()

	path := ""
	if entry != nil {
		path = entry.Path
	}

	return gmenu.Run(&config.Config, execute, path, runInTerminal, waitUntilFinished)
}
