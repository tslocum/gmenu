package main

import (
	"log"
	"os"

	"github.com/gotk3/gotk3/gdk"
	"github.com/gotk3/gotk3/gtk"
)

func initWindow(application *gtk.Application) *gtk.ApplicationWindow {
	w, err := gtk.ApplicationWindowNew(application)
	if err != nil {
		log.Fatal("failed to create window:", err)
	}

	_, err = w.Connect("key-press-event", handleKeybinding)
	if err != nil {
		log.Fatal("failed to connect key-press-event:", err)
	}

	_, err = w.Connect("destroy", func() {
		os.Exit(0)
	})
	if err != nil {
		log.Fatal("failed to create application window:", err)
	}

	w.SetTitle("gmenu")
	w.SetDecorated(false)
	w.SetBorderWidth(0)
	w.Stick()
	w.SetKeepAbove(true)
	w.SetTypeHint(gdk.WINDOW_TYPE_HINT_UTILITY)

	if !config.Fullscreen {
		w.SetResizable(config.Resizable)
		w.SetSizeRequest(config.Width, config.Height)
		w.SetPosition(gtk.WIN_POS_CENTER)
	} else {
		w.Fullscreen()
	}

	return w
}

func handleKeybinding(_ *gtk.ApplicationWindow, ev *gdk.Event) bool {
	keyEvent := &gdk.EventKey{ev}
	switch keyEvent.KeyVal() {
	case gdk.KEY_Up, gdk.KEY_Down:
		offset := -1
		if keyEvent.KeyVal() == gdk.KEY_Down {
			offset = 1
		}

		index := 0
		row := listBox.GetSelectedRow()
		if row != nil {
			index = row.GetIndex()
		}

		row = listBox.GetRowAtIndex(index + offset)
		if row != nil {
			listBox.SelectRow(row)
			row.GrabFocus()
			inputView.GrabFocus()
		}

		return true
	case gdk.KEY_Return, gdk.KEY_KP_Enter, gdk.KEY_ISO_Enter, gdk.KEY_3270_Enter:
		runInTerminal := keyEvent.State()&uint(gdk.MOD1_MASK) > 0

		err := listSelect(inputView, runInTerminal)
		if err != nil {
			log.Fatal(err)
		}

		return true
	case gdk.KEY_Escape:
		os.Exit(0)
	}

	return false
}
