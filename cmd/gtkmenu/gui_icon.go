package main

import (
	"crypto/md5"
	"fmt"
	"log"
	"os"
	"path"

	"github.com/gotk3/gotk3/gdk"
	"github.com/gotk3/gotk3/gtk"
	"gitlab.com/tslocum/desktop"
	"gitlab.com/tslocum/gmenu/pkg/gmenu"
)

const cachedIconCompressionLevel = 3

var (
	iconTheme    *gtk.IconTheme
	iconCacheDir string
)

func loadIcon(icon string) (*gdk.Pixbuf, error) {
	cachedIcon := path.Join(iconCacheDir, fmt.Sprintf("%x.png", md5.Sum([]byte(icon))))

	var (
		pbuf *gdk.Pixbuf
		err  error
	)
	if _, statErr := os.Stat(cachedIcon); !os.IsNotExist(statErr) {
		pbuf, err = gdk.PixbufNewFromFileAtSize(cachedIcon, iconSize, iconSize)
	}
	if pbuf == nil || err != nil {
		if path.IsAbs(icon) {
			pbuf, err = gdk.PixbufNewFromFileAtSize(icon, iconSize, iconSize)
		} else {
			pbuf, err = iconTheme.LoadIcon(icon, iconSize, gtk.ICON_LOOKUP_USE_BUILTIN)
		}
	}
	if err != nil {
		return nil, err
	}

	if pbuf.GetWidth() != iconSize || pbuf.GetHeight() != iconSize {
		pbuf, _ = pbuf.ScaleSimple(iconSize, iconSize, gdk.INTERP_BILINEAR)
	}

	go cacheIcon(cachedIcon, pbuf)

	return pbuf, nil
}

func cacheIcon(cachedIcon string, pbuf *gdk.Pixbuf) {
	f, err := os.OpenFile(cachedIcon, os.O_CREATE|os.O_RDWR, 0644)
	if err != nil {
		log.Println(err)
		return
	}

	pbuf.SavePNG(f.Name(), cachedIconCompressionLevel)
}

func fallbackIcon(entry *gmenu.ListEntry) string {
	if entry.Entry == nil {
		return "utilities-terminal"
	} else if entry.Type == desktop.Application {
		return "application-x-executable"
	}

	return "text-html"
}

func loadIconImage(img *gtk.Image, entry *gmenu.ListEntry) {
	var (
		pbuf *gdk.Pixbuf
		err  error
	)
	if entry.Entry != nil && entry.Icon != "" {
		pbuf, err = loadIcon(entry.Icon)
	}
	if pbuf == nil || err != nil {
		pbuf, err = loadIcon(fallbackIcon(entry))
	}
	if err != nil {
		log.Printf("failed to load entry icon %s: %s", entry.Icon, err)
	}

	img.SetFromPixbuf(pbuf)
}
